Template for Thesis at Temple University
========================================

Introduction
------------

Since the graduate school does not provide a latex template, I thought I would 
create one and share it with everyone to help cut down on the tedious task of 
formatting your thesis.

This work is based off of the very nicely documented [Purdue University 
thesis](https://engineering.purdue.edu/~mark/puthesis/)
class by Mark Senn.  Please thank him. 

Ideally the Temple University Grad School would maintain and update with their 
latest formatting requirements (since almost everybody uses latex). Perhaps if 
enough people ask for latex templates they will make one.

 - Currently on works for physics (sorry, feel free to modify the code)
 - Last updated to follow 2015 [Dissertation and Thesis 
   Handbook](http://www.temple.edu/dissertationhandbook/index.htm)

The Template
------------

In the template folder you will find a very nice starting point for your 
thesis.

Usage
-----

Note: This has only been used on Linux systems. If you find bugs, please fix 
and/or report them via github.



